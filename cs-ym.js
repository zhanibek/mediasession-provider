/**
 * Copyright 2018 Zhanibek Adilbekov <zhanibek.adilbekov@protonmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

window.eval(`

function waitInitExternalAPI() {
  if (window.externalAPI) {
    const api = window.externalAPI;
    const ms = navigator.mediaSession;

    const updateMetadata = () => {
      const currentTrack = api.getCurrentTrack();
      if (currentTrack) {
        ms.metadata = new window.MediaMetadata({
          title: currentTrack.title,
          artist: currentTrack.artists?.map(a => a.title).join(' & '),
          album: currentTrack.album?.title,
          artwork: currentTrack.cover ? [
            { src: 'https://' + currentTrack.cover.replace('%%', '400x400'), sizes: '400x400', type: 'image/jpeg' },
            { src: 'https://' + currentTrack.cover.replace('%%', '300x300'), sizes: '300x300', type: 'image/jpeg' },
            { src: 'https://' + currentTrack.cover.replace('%%', '200x200'), sizes: '200x200', type: 'image/jpeg' },
            { src: 'https://' + currentTrack.cover.replace('%%', '100x100'), sizes: '100x100', type: 'image/jpeg' },
            { src: 'https://' + currentTrack.cover.replace('%%', '80x80'), sizes: '80x80', type: 'image/jpeg' },
            { src: 'https://' + currentTrack.cover.replace('%%', '50x50'), sizes: '50x50', type: 'image/jpeg' },
            { src: 'https://' + currentTrack.cover.replace('%%', '30x30'), sizes: '30x30', type: 'image/jpeg' },
          ] : [
            { src: 'https://music.yandex.com/blocks/meta/i/og-image.png', sizes: '1024x1024', type: 'image/png' },
          ],
        });
      }
    };

    api.on(api.EVENT_STATE, () => {
      if (api.isPlaying()) {
        ms.playbackState = 'playing';
        updateMetadata();
      }
      else {
        ms.playbackState = 'paused';
      }
    });
    api.on(api.EVENT_TRACK, updateMetadata);

    ms.setActionHandler('play', () => { api.togglePause(false) });
    ms.setActionHandler('pause', () => { api.togglePause(true) });

    ms.setActionHandler('seekbackward', () => {
      api.setPosition(api.getProgress().position - 5);
    });
    ms.setActionHandler('seekforward', () => {
      api.setPosition(api.getProgress().position + 5);
    });

    ms.setActionHandler('previoustrack', () => {
      api.prev();
      api.setPosition(0);
    });
    ms.setActionHandler('nexttrack', () => {
      api.next();
      api.setPosition(0);
    });
  }
  else {
    window.setTimeout(waitInitExternalAPI, 500);
  }
}

if (!window.mspTimeoutId) {
  window.mspTimeoutId = window.setTimeout(waitInitExternalAPI, 500);
}

`);
