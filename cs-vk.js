/**
 * Copyright 2018 Zhanibek Adilbekov <zhanibek.adilbekov@protonmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

window.eval(`

function waitAudioPlayer() {
  if (window.getAudioPlayer) {
    const player = window.getAudioPlayer();
    const ms = navigator.mediaSession;

    player.eventBus.subscriptions.start.forEach(s => {
      player.eventBus.unsubscribe('start', s);
    });
    player.connectEvents();

    const updateMetadata = () => {
      const currentTrack = window.AudioUtils.asObject(player.getCurrentAudio());
      const artwork = [];
      if (currentTrack.coverUrl_p) {
        artwork.push({ src: currentTrack.coverUrl_p, sizes: '160x160' });
      }
      if (currentTrack.coverUrl_s) {
        artwork.push({ src: currentTrack.coverUrl_s, sizes: '80x80' });
      }
      if (artwork.length == 0) {
        artwork.push({ src: 'https://vk.com/images/icons/pwa/apple/default.png', sizes: '512x512', type: 'image/png' });
      }
      const decode = s => {
        s = (s || '').replace(/<[^>]*>?/gm, '');
        return window.decodeHTMLEntities ? window.decodeHTMLEntities(s) : s;
      };
      const metadata = {
        title: decode(currentTrack.title + (currentTrack.subTitle ? ' (' + currentTrack.subTitle + ')' : '')),
        artist: decode(currentTrack.performer),
        artwork,
      };
      const playlist = player.getCurrentPlaylist();
      if (playlist && playlist.getTitle) {
        metadata.album = decode(playlist.getTitle() || '');
      }
      ms.metadata = new window.MediaMetadata(metadata);
    }

    player.subscribe('start', () => {
      ms.playbackState = 'playing';

      updateMetadata();

      ms.setActionHandler('play', () => { player.play(); });
      ms.setActionHandler('pause', () => { player.pause(); });

      ms.setActionHandler('seekbackward', () => { player.seekCurrentAudio(false); });
      ms.setActionHandler('seekforward', () => { player.seekCurrentAudio(true); });

      ms.setActionHandler('previoustrack', () => { player.playPrev(); });
      ms.setActionHandler('nexttrack', () => { player.playNext(); });
    });
    player.subscribe('pause', () => {
      ms.playbackState = 'paused';
    });

  }
  else {
    window.setTimeout(waitAudioPlayer, 500);
  }
}

if (!window.mspTimeoutId) {
  window.mspTimeoutId = window.setTimeout(waitAudioPlayer, 500);
}

`);
