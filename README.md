# MediaSession Provider

[Get it for Firefox](https://addons.mozilla.org/en-US/firefox/addon/mediasession-provider/)

[Get it for Chrome](https://chrome.google.com/webstore/detail/mediasession-provider/ejiigemgllfgaaandolnapdilkgohkhg)

This add-on provides [MediaSession](https://developer.mozilla.org/en-US/docs/Web/API/MediaSession) data for [VK audio](https://vk.com/audio) and [Yandex.Music](https://music.yandex.com). Which can be handled by software that supports it.

Firefox has native support (since Firefox 75) for [MPRIS](https://wiki.archlinux.org/title/MPRIS) interface, which allows control media (next track, previous track, play/pause, shows album art) using desktop widgets (see example in picture below) or multimedia keyboard keys.

_NOTE: I have some glitches with Plasma Browser Integration and Yandex.Music. Play/Pause bursts multiple times when called once. Disable "Media Controls" in Plasma Integration add-on, and enable native support in Firefox in `Preferences`→``General`→`Browsing`→`Control media via keyboard, headset, or virtual interface_

![Add-on image on AMO](https://addons.cdn.mozilla.net/user-media/previews/full/211/211724.png?modified=1548492706)
