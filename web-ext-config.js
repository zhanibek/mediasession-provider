module.exports = {
    ignoreFiles: [
        'web-ext-config.js',
        'CHANGELOG.md',
        'LICENSE.txt',
        'README.md',
        'icon.svg',
    ],
};
